Name:           powerapi
Version:        1.0.1
Release:        2
Summary:        The ability to support openEuler power consumption control.

License:        MulanPSL-2.0
URL:            https://gitee.com/openeuler/powerapi/
Source0:        %{name}-%{version}.tar.gz
Patch0:         powerapi-0.patch

BuildRequires:  gcc,cmake,zip,unzip

%description
Including a power API SO and the Power API Service.

%package -n powerapi-devel
Summary:        The ability to support openEuler power consumption control.
BuildRequires:  gcc,cmake,zip,unzip

%description -n powerapi-devel
Contains the client and server components and API header files for powerapi integration development.

%prep
%autosetup -p1

%build
sh build.sh release

%install
install -d %{buildroot}/%{_sysconfdir}/sysconfig/pwrapis
cp release/pwrapis/conf/pwrapis_config.ini %{buildroot}/%{_sysconfdir}/sysconfig/pwrapis
install -d %{buildroot}/%{_sbindir}
cp release/pwrapis/pwrapis %{buildroot}/%{_sbindir}
install -d %{buildroot}/%{_sysconfdir}/systemd/system
cp pwrapis/pwrapis.service %{buildroot}/%{_sysconfdir}/systemd/system
install -d %{buildroot}/%{_libdir}
cp release/pwrapic/lib/libpwrapi.so %{buildroot}/%{_libdir}
install -d %{buildroot}/%{_includedir}/pwrapic
cp -r release/pwrapic/inc/* %{buildroot}/%{_includedir}/pwrapic

%post
systemctl start pwrapis

%preun
systemctl stop pwrapis

%post -n powerapi-devel
systemctl start pwrapis

%preun -n powerapi-devel
systemctl stop pwrapis

%files
%{_sbindir}/pwrapis
%{_libdir}/libpwrapi.so
%dir %{_sysconfdir}/sysconfig/pwrapis
%{_sysconfdir}/sysconfig/pwrapis/*
%{_sysconfdir}/systemd/system/pwrapis.service

%files -n powerapi-devel
%{_sbindir}/pwrapis
%{_libdir}/libpwrapi.so
%dir %{_sysconfdir}/sysconfig/pwrapis
%{_sysconfdir}/sysconfig/pwrapis/*
%dir %{_includedir}/pwrapic
%{_includedir}/pwrapic/*
%{_sysconfdir}/systemd/system/pwrapis.service

%changelog
* Wed Dec  27 2023 xuchongyu <xuchongyu@huawei.com> - 1.0.1-2
- Solve the problem that the client is actively disconnected and the server is incorrect in clearing resources

* Sat Dec  23 2023 queyanwen <queyanwen@huawei.com> - 1.0.1-1
- Change the version number for the first releasing

* Fri Dec  22 2023 queyanwen <queyanwen@huawei.com> - 1.0.0-9
- Solve the problem that the client will coredump if the user is not in the white list

* Fri Dec  22 2023 xuchongyu <xuchongyu@huawei.com> - 1.0.0-8
- Solve the problem that the automatic generation permission of the sockfile path on the server is insufficient
- And ordinary users cannot connect

* Thu Dec  21 2023 xuchongyu <xuchongyu@huawei.com> - 1.0.0-7
- Solve the problem that the illegal frequency range of the PWR_CPU_SetFreq interface is inconsistent with the illegal governor error code
- And the error code returned by the interface with the set frequency range is inappropriate

* Tue Dec  19 2023 xuchongyu <xuchongyu@huawei.com> - 1.0.0-6
- Automatically create the sockfile directory if it does not exist

* Mon Dec  18 2023 queyanwen <queyanwen@huawei.com> - 1.0.0-5
- Solve the coredump problem during CI compilation
- Solve the garbled characters caused by uninitialized string variables

* Mon Dec  18 2023 queyanwen <queyanwen@huawei.com> - 1.0.0-4
- Modify powerapi.spec to support the debug package

* Mon Dec  18 2023 xuchongyu <xuchongyu@huawei.com> - 1.0.0-3
- update souce code：retain debug compilation mode

* Fri Dec  15 2023 xuchongyu <xuchongyu@huawei.com> - 1.0.0-2
- add pwrapis.service

* Tue Dec  5 2023 xuchongyu <xuchongyu@huawei.com> - 1.0.0-1
- init powerapi
